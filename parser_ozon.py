import csv
from transliterate import translit
import requests
from bs4 import BeautifulSoup

result = []
keys = {}

articles = []

length = len(articles)
count = 0
for article in articles:
    data = {}
    data['article'] = article
    keys['article'] = 0
    url = 'https://www.ozon.ru/context/detail/id/' + str(article) + '/'
    data['url'] = url
    keys['url'] = 0
    page = requests.get(url).text
    soup = BeautifulSoup(page, "html.parser")
    name = soup.find('h1', {'class': 'b2u6'}).text.split()
    if len(name) > 1:
        name = name[1]
    else:
        name = 'BRAVE LION'
    data['name'] = name
    keys['name'] = 0
    category = soup.find('h1', {'class': 'b2u6'}).text.split()[0]
    data['category'] = category
    keys['category'] = 0
    symbol_code = '-'.join(name.lower().split()) \
                  + '-' \
                  + (translit(category, 'ru', reversed=True)).lstrip().lower() \
                  + '-' + str(article)
    data['symbol_code'] = symbol_code
    keys['symbol_code'] = 0
    params = soup.findAll('dl', {'class': 'b3w9'})
    for param in params:
        property = param.contents[0].text.strip()
        value = param.contents[2].text.strip()
        data[property] = value
        keys[property] = 0
    if soup.find('span', {'class': 'b3c b3d2'}) is not None:
        cost = ''.join(soup.find('span', {'class': 'b3c b3d2'}).text.strip().replace('₽', '').split())
    else:
        cost = '3651'

    data['cost'] = cost
    keys['cost'] = 0
    if soup.find('div', {'id': 'section-description'}) is not None:
        description = '"' + soup.find('div', {'id': 'section-description'}).text.strip() + '"'
    else:
        description = ''
    data['description'] = description
    keys['description'] = 0
    images = soup.find('div', {'class': 'portal-content-list'}).findAll('img')
    array_images = []
    for image in images:
        array_images.append(image.get('src').replace('c50', 'c1200'))
    images = '; '.join(array_images)
    data['images'] = images
    keys['images'] = 0
    result.append(data)
    print(str(count + 1) + ' из ' + str(length))
    count = count + 1

filename = 'Озон.csv'

with open(filename, 'w') as csvfile:
    fieldnames = keys.keys()
    writer = csv.DictWriter(csvfile, fieldnames)
    writer.writeheader()
    writer.writerows(result)

print("writing complete")
