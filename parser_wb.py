import csv
from transliterate import translit
import requests
from bs4 import BeautifulSoup

result = []
keys = {}

articles = []
length = len(articles)
count = 0
for article in articles:
    data = {}
    data['article'] = article
    keys['article'] = 0
    url = 'https://www.wildberries.ru/catalog/' + str(article) + '/detail.aspx'
    data['url'] = url
    keys['url'] = 0
    page = requests.get(url).text
    soup = BeautifulSoup(page, "html.parser")
    name = soup.find('span', {'class': 'brand'}).text.strip()
    data['name'] = name
    keys['name'] = 0
    category = soup.find('span', {'class': 'name'}).text.strip()
    data['category'] = category
    keys['category'] = 0
    symbol_code = '-'.join(name.lower().split()) \
                  + '-' \
                  + (translit(category, 'ru', reversed=True)).lstrip().lower() \
                  + '-' + str(article)
    data['symbol_code'] = symbol_code
    keys['symbol_code'] = 0
    params = soup.findAll('div', {'class': 'pp'})
    for param in params:
        property = param.contents[0].text
        value = param.contents[1].text.lstrip()
        data[property] = value
        keys[property] = 0
    color = soup.find('span', {'class': 'color'}).text.lstrip().title()
    data['color'] = color
    keys['color'] = 0
    if soup.find('span', {'class': 'final-cost'}) is not None:
        cost = ''.join(soup.find('span', {'class': 'final-cost'}).text.strip().replace('₽', '').split())
    else:
        cost = '3651'

    data['cost'] = cost
    keys['cost'] = 0
    if soup.find('div', {'class': 'description-text'}) is not None:
        description = '"' + soup.find('div', {'class': 'description-text'}).text.strip() + '"'
    else:
        description = ''
    data['description'] = description
    keys['description'] = 0
    images = soup.find('div', {'class': 'photo-items'})
    array_images = []
    for image in images:
        if str(image.find('a')) != '-1':
            array_images.append(image.find('a').get('href').replace('//', ''))

    images = ';'.join(array_images)
    data['images'] = images
    keys['images'] = 0
    result.append(data)
    print(str(count + 1) + ' из ' + str(length))
    count = count + 1

filename = 'Леон5.csv'

with open(filename, 'w') as csvfile:
    fieldnames = keys.keys()
    writer = csv.DictWriter(csvfile, fieldnames)
    writer.writeheader()
    writer.writerows(result)

print("writing complete")
