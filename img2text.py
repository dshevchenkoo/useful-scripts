from PIL import Image, ImageDraw, ImageOps
import cv2
import pytesseract

image = Image.open("test3.jpg") #Открываем изображение.
draw = ImageDraw.Draw(image) #Создаем инструмент для рисования.
width = image.size[0]  # Определяем ширину.
height = image.size[1]  # Определяем высоту.
pix = image.load()  # Выгружаем значения пикселей.

for i in range(width):
    for j in range(height):
        a = pix[i, j][0]
        b = pix[i, j][1]
        c = pix[i, j][2]
        S = (a + b + c) // 3
        draw.point((i, j), (S, S, S))
image.save('result.png')
# inverted_image = ImageOps.invert(image)

# inverted_image.save('new_name.png')
# print(cv2.__version__)
img = cv2.imread(r'result.png')
print(pytesseract.image_to_string(img, lang='eng', config='--psm 6 -c tessedit_char_whitelist=0123456789'))