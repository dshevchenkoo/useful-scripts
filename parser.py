import csv
import requests
from bs4 import BeautifulSoup

result = {}

def get_data(pages, type, url):
    for i in range(1, pages):

        if i > 1:
            url1 = url + '-page' + str(i)
        else:
            url1 = url
        page = requests.get(url1).text
        soup = BeautifulSoup(page, "html.parser")
        divs = soup.findAll('a', {'class': 'product-item__name'})
        for a in divs:
            text_title = a.text
            if text_title in result:
                if type not in result[text_title]:
                    result[text_title].append(type)
            else:
                result[text_title] = []
                result[text_title].append(type)
        print(type + ' ' + str(i))

get_data(18, 'type', 'url')

filename = 'file.csv'

with open(filename, "w", newline="") as file:
    data = []
    for item in result:
        elem = [item]
        for value in result[item]:
            elem.append(value)
        data.append(elem)

    writer = csv.writer(file)
    writer.writerows(data)
